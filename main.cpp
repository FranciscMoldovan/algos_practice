#include <iostream>
#include <vector>

void exchange(int* x, int* y)
{
    *x = *x ^ *y;
    *y = *x ^ *y;
    *x = *x ^ *y;
}

void bubble_sort(std::vector<int>& array)
{
    for (size_t i = 0; i < array.size(); ++i)
    {
        for(size_t j = i; j < array.size(); j++)
            if(array[i]>array[j])
            {
                exchange(&array[i], &array[j]);
            }
    }
}

void insertion_sort(std::vector<int>& array)
{
    int key;
    for(size_t i = 1; i < array.size(); i++)
    {
        key = array[i];
        // now place the key where it belongs
        int j = i - 1;
        while ( j >= 0 && array[j] > key)
        {
            array[j+1] = array[j];
            j = j-1;
        }
        array[j+1] = key;
    }
}

int selection_sort(std::vector<int>& array)
{
    int min_idx = 0;
    size_t i = 0;
    for(size_t j = 0; j < array.size(); j++)
    {
        i = j;
        // look for the min
        while( i < array.size())
        {
            if (array[i] < array[min_idx])
            {
                min_idx = i;
            }
            i++;
        }
        exchange(&array[j], &array[min_idx]);
    }
}

int main()
{
    int x = 5, y = 7;
    exchange(&x, &y);

    std::vector<int> a = {72, 5, 6, 3, 1, 2, 54, 7, 8, 9};
//    bubble_sort(a);
//    insertion_sort(a);
//    selection_sort(a);

    return 0;
}

